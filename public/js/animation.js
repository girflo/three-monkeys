import * as THREE from '../js/three/three.module.js';
import { OBJLoader } from '../js/three/libs/OBJLoader.js';
import { LightProbeGenerator } from '../js/three/libs/LightProbeGenerator.js';
import { OrbitControls } from '../js/three/libs/OrbitControls.js';
import { TransformControls } from '../js/three/libs/TransformControls.js';

export function run () {
  var container;
  var scene, renderer, control, orbit;
  var cameraPersp, cameraOrtho, currentCamera;
  var mouseX = 0, mouseY = 0;
  var windowHalfX = window.innerWidth / 2;
  var windowHalfY = window.innerHeight / 2;
  var objects = [];
  var sceneLoaded = false;
  var lightProbeExist = false;
  var selectedObjectIndex;

  init();
  render();

  function init() {
    container = document.createElement( 'div' );
    document.body.appendChild( container );
    scene = new THREE.Scene();
    scene.add( new THREE.GridHelper( 1000, 10, 0x888888, 0x444444 ) );

    const aspect = window.innerWidth / window.innerHeight;

    cameraPersp = new THREE.PerspectiveCamera( 35, aspect, 0.01, 30000 );
    cameraOrtho = new THREE.OrthographicCamera( - 600 * aspect, 600 * aspect, 600, - 600, 0.01, 30000 );
    currentCamera = cameraPersp;

    currentCamera.position.set( 1000, 500, 1000 );
    currentCamera.lookAt( 0, 200, 0 );

    renderer = new THREE.WebGLRenderer( { alpha: false, antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );

    orbit = new OrbitControls( currentCamera, renderer.domElement );
    orbit.update();
    orbit.addEventListener( 'change', render );

    control = new TransformControls( currentCamera, renderer.domElement );
    control.addEventListener( 'change', render );
    control.addEventListener( 'dragging-changed', function ( event ) {
      orbit.enabled = ! event.value;
    } );
    scene.add( control );

    var directionalLight = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set( 0, 0, 1 );
    scene.add( directionalLight );

    var directionalLightBack = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set( 30, 30, -1 );
    scene.add( directionalLightBack );

    var manager = new THREE.LoadingManager();
    var baseUrl = "./obj/"
    loadObj(0, "sam", manager, baseUrl + "suzanne.obj");
    loadObj(1, "giusy", manager, baseUrl + "suzanne_reversed.obj");

    renderer.toneMapping = THREE.NoToneMapping;
    renderer.outputEncoding = THREE.sRGBEncoding;

    window.addEventListener( 'resize', onWindowResize, false );

    document.getElementById("translate-button").onclick = function(event){
      removeActiveButton()
      addActiveButton("translate-button")
      control.setMode( "translate" );
    }

    document.getElementById("rotate-button").onclick = function(event){
      removeActiveButton()
      addActiveButton("rotate-button")
      control.setMode( "rotate" );
    }

    document.getElementById("scale-button").onclick = function(event){
      removeActiveButton()
      addActiveButton("scale-button")
      control.setMode( "scale" );
    }

    document.getElementById("focus-button").onclick = function(event){
      switchControledObj();
    }
  }

  function removeActiveButton() {
    var elements = document.getElementsByClassName('button-smartphone');
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove('active')
    }
  }

  function addActiveButton(id) {
    var element = document.getElementById(id);
    element.classList.add("active");
  }

  function getMousePosition( dom, x, y ) {
    var rect = dom.getBoundingClientRect();
    return [ ( x - rect.left ) / rect.width, ( y - rect.top ) / rect.height ];
  }

  function onWindowResize() {
    const aspect = window.innerWidth / window.innerHeight;
    cameraPersp.aspect = aspect;
    cameraPersp.updateProjectionMatrix();
    cameraOrtho.left = cameraOrtho.bottom * aspect;
    cameraOrtho.right = cameraOrtho.top * aspect;
    cameraOrtho.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    render();
  }

  function render() {
    renderer.render( scene, currentCamera );
  }

  function getLightProbe() {
    if (lightProbeExist == true) {
      return scene.children[scene.children.length - 1]
    } else {
      lightProbeExist = true;
      var lightProbe = new THREE.LightProbe();
      scene.add( lightProbe );
      return lightProbe
    }
  }

  function loadObj(index, name, manager, objUrl) {
    var texCompleted = false;
    var objCompleted = false;
    var loadCompleted = false;
    var group = new THREE.Group();
    var lightProbe = getLightProbe();

    var onProgress = function ( xhr ) {
      if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        if(percentComplete >= 100) {
          objCompleted = true;
          if( texCompleted) group.loaded=true;
          if(!loadCompleted) {
            checkLoading();
            loadCompleted = true;
          }
        }
      }
    };
    var onError = function ( xhr ) {
    };

    var loader = new OBJLoader( manager );
    loader.load(objUrl, function ( object ) {
      var mesh = object.children[ 0 ];
      mesh.geometry = new THREE.Geometry().fromBufferGeometry( mesh.geometry );
      mesh.geometry.mergeVertices();
      mesh.geometry.computeVertexNormals();
      mesh.geometry = new THREE.BufferGeometry().fromGeometry( mesh.geometry );

      var genCubeUrls = function ( prefix, postfix ) {
	return [
	  prefix + 'px' + postfix, prefix + 'nx' + postfix,
	  prefix + 'py' + postfix, prefix + 'ny' + postfix,
	  prefix + 'pz' + postfix, prefix + 'nz' + postfix
	];
      };
      var urls = genCubeUrls( './textures/cube/4/', '.png' );

      new THREE.CubeTextureLoader().load( urls,  function ( cubeTexture ) {
        cubeTexture.encoding = THREE.sRGBEncoding;
        lightProbe.copy( LightProbeGenerator.fromCubeTexture( cubeTexture ) );
        var mat = new THREE.MeshStandardMaterial( { color: 0xffffff,
                                                    metalness: 1,
                                                    roughness: 0.1,
                                                    envMap: cubeTexture,
                                                    envMapIntensity: 1 } );
        switch (name){
        case "sam":
          mat.shininess= 10;
	  setMat(object,mat);
	  object.scale.set(50,50,50);
	  group.add(object);
	  break;
        default:
          mat.shininess= 10;
	  setMat(object,mat);
	  object.scale.set(50,50,50);
	  group.add(object);
        }
        render();
      } );
    }, onProgress, onError );
    group.show = true;
    objects.push(group);
  }

  function setMat(object,mat){
    object.traverse( function ( child ) {
      if ( child instanceof THREE.Mesh ) {
	child.material = mat;
      }
    } );
  }

  function spawnMulitpleObj(index1, index2) {
    scene.add(objects[index1]);
    scene.add(objects[index2]);
    attachControlToObj(index2, "➤ Ennazus");
  }

  function attachControlToObj(index, name) {
    selectedObjectIndex = index;
    document.getElementById('focus-button').innerHTML = name;
    control.attach( objects[index] );
    if (index == 0) {
      setPurpleColor();
    } else {
      setGreenColor();
    }
  }

  function setGreenColor() {
    document.getElementById('three-container').classList.remove("purple");
    document.getElementById('translate-button-img').src = "images/green/translate.svg";
    document.getElementById('rotate-button-img').src = "images/green/rotate.svg";
    document.getElementById('scale-button-img').src = "images/green/scale.svg";
  }

  function setPurpleColor() {
    document.getElementById('three-container').classList.add("purple");
    document.getElementById('translate-button-img').src = "images/purple/translate.svg";
    document.getElementById('rotate-button-img').src = "images/purple/rotate.svg";
    document.getElementById('scale-button-img').src = "images/purple/scale.svg";
  }

  function switchControledObj() {
    if (selectedObjectIndex == 0) {
      attachControlToObj(1, "➤ Ennazus");
    } else {
      attachControlToObj(0, "➤ Suzanne");
    }
  }

  var objLoaded = 0;

  function checkLoading() {
    objLoaded ++;
    if(objLoaded>=objects.length) {
      loadFirstModel(0);
    }
  }

  function loadFirstModel(index){
    if(sceneLoaded) return;
    sceneLoaded = true;
    spawnMulitpleObj(0, 1);
  }
}

window.mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
